---
title: "Analyse des résultats"
author: '@sycom'
date: "14/04/2022"
output: html_document
---

```{r, setup, include=F}
library(tidyverse)
knitr::opts_chunk$set(echo=F)
source("R/candidats_palettes.R", encoding="UTF-8")
v_dep <- read.csv("import/presid_2022-T1-dep.csv", encoding="UTF-8")
v_com <- read.csv("import/presid_2022-T1-com.csv", encoding="UTF-8",
                  colClasses=c("character","character","character",
                               "integer","integer","integer","integer",
                               "integer","integer","integer","integer",
                               "integer","integer","integer","integer",
                               "integer","integer","integer","integer"))
v_com$com <- paste0(v_com$dep,v_com$com)
```

## Les résultats par zone scolaire

```{r, zones_scol, echo=F}
z_abc <- read.csv("data/zones_scolaires_ABC.csv", encoding="UTF-8")
v_dep_ <- merge(v_dep, z_abc, by.x="DEP", by.y="dep")
v_dep_l <- v_dep_ %>% pivot_longer(names_to="candidat",
                                 cols=c(4:18), values_to="voix")
v_dep_ <- v_dep_l %>% group_by(zone, candidat) %>%
  summarise(voix=sum(voix)) %>%
  mutate(part=voix/sum(voix)*100)
v_dep_$candidat <- factor(v_dep_$candidat, levels=liste_res(candidat2))
v_dep_ %>% ggplot(aes(x=part, y=zone, fill=candidat)) +
  geom_bar(stat="identity") +
#  geom_text(aes(label=round(10*part)/10), position=position_stack(vjust=0.5), colour="#555555") +
  theme_minimal() +
  scale_fill_manual(values=pal_res()) 
```

L'abstention a été plus importante dans la zone B (25 %) que dans les zones A et C (22 % et 23,1 %). Cela peut toutefois être lié à des caractéristiques structurelles des différentes zones et pas seulement aux vacances (commencées dans la zone B à la date du scrutin). D'ailleurs, la Corse (37,3 % d'abstention) n'était pas en période de vacances scolaires.

Dans le mesure où, pour le deuxième tour, les trois zones seront en vacances, cette question n'est pas neutre.

## Regard sur les corrélations

### Au niveau départemental

```{r, corrdep, echo=FALSE}
v_dep_t <- v_dep
for(c in c(4:18)) v_dep_t[,c(c)] <- v_dep_t[,c(c)]/v_dep_t[,c(3)]*100

correl_ <- cor(v_dep_t[,c(3:18)])
corrplot::corrplot(correl_, "color", tl.col="#000000dd")
```
Les plus fortes corrélations négatives au niveau départemental sont

* abstention / Hidalgo et dans une moindre mesure Jadot et Macron
* inscrits / Hidalgo et dans une moindre mesure Macron et Jadot
* Le Pen / Jadot ou Mélanchon et dans une moindre mesure Macron
* Zemmour / Arthaud et dans une moindre mesure Poutou

Les plus fortes corrélations positives sont sans doute plus à déduire de ces corrélations négatives : les territoires le plus peuplés, et où on s'est le moins abstenu ont plus voté pour Macron, Jadot ou Hidalgo.

Le Pen et Zemmour sont les seuls à avoir une corrélation positive avec l'abstention, signe que la mobilisation de leurs électeurs a été bonne partout en France.

### Au niveau communal

```{r, corrcom, echo=FALSE}
v_com_t <- v_com
for(c in c(5:19)) v_com_t[,c(c)] <- v_com_t[,c(c)]/v_com_t[,c(4)]*100
correl_ <- cor(v_com_t[,c(4:19)])
corrplot::corrplot(correl_, "color", tl.col="#000000dd")
```

* L'opposition la plus forte est entre Le Pen et Mélenchon ou Jadot
* L'abstention semble avoir été surtout préjudiciable à Macron et dans une moindre mesure Jadot
* Les communes comportant le plus d'inscrit ont connu plus d'abstention et un vote plus important pour Mélenchon et Jadot

### Une typologie des communes

```{r, typo, echo=F}
library(FactoMineR)
library(factoextra)
# ACP
v_com.pca <- PCA(v_com_t[,c(4:19)], quanti.sup=c(1), graph=F)
# CAH avec un premier traitement kmeans
v_com.cah <- HCPC(v_com.pca, kk=256, min=4, max=7, graph=F)
```

```{r, typo.dend, echo=F}
# visualisation de la CAH
# dendogramme
fviz_dend(v_com.cah, 
          cex = 0.7,                     
          palette = "jco",               
          rect = TRUE, rect_fill = TRUE,
          rect_border = "jco",         
          )
# clusters dans les deux premières dimensions
fviz_cluster(v_com.cah,
             repel = TRUE,            
             show.clust.cent = TRUE, 
             palette = "jco",        
             ggtheme = theme_minimal(),
             main = "Factor map"
             )
# sauvegarde de la CAH en csv
cah_ <- cbind(v_com[,c(2)],v_com.cah$data.clust)
colnames(cah_)[1] <- "depcom"
write.csv(cah_, "export/presid_2022-T1-com-clust.csv", fileEncoding="UTF-8")
```

```{r, typo.desc, include=F}
v_com.cah$desc.var$quanti
```

En utilisant les parts des inscrits au scrutin pour chaque commune, par une analyse en compostantes principales puis classification hiérarchique ascendante, il est possible de construire une typologie des communes pour ce premier tour en 4 classes :

* classe 1 : abstention moyenne à faible, vote à droite plus marqué que la moyenne des communes (Le Pen +++, Zemmour ++, Dupont-Aignan ++, Pécresse +). Dans cette classe, le vote pour Arthaud est également fort. Le vote pour Mélenchon, Jadot, Hidalgo voire Macron est plus faible que la moyenne. Les communes ont moins d'inscrits que la moyenne.
* classe 2 : communes à abstention très forte. Cette abstention se fait au détriment de tous les candidats à l'exception de Mélenchon, et particulièrement Macron, Pécresse, dans une moindre mesure Jadot, Zemmour, Lasalle. Arthaud, Poutou et Le Pen sont partiellement épargné·e·s. Ces communes ont plus d'inscrits que la moyenne.
* classe 3 : communes à abstention modéré et dont le vote se porte vers le centre. Le vote pour Macron et Jadot y est fort par rapport à la moyenne, celui pour Mélenchon ou Pécresse à peine supérieur à la moyenne. En revanche Lasalle, Le Pen, Roussel et Zemmour y sont plutôt faibles. Ces communes ont un peu plus d'inscrits en moyenne.
* classe 4 : abstention faible, mobilisé plutôt à gauche. Les scores pour Hidalgo, Mélanchon, Roussel et Poutou y sont plus fort qu'en moyenne nationale. Le vote pour Lasalle est également très élevé dans cette classe. Le vote pour Le Pen, Macron, Zemmour ou Dupont-Aignan est plus faible. Le nombre d'inscrit est également plus faible.

#### Répartition spatiale des 4 classes

![carte des 4 classes de la typologie](./export/typologie_choro.png)

#### Répartition des 4 classes en vote

![carte des 4 classes de la typologie](./export/typologie_propo.png)