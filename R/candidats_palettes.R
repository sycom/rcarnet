# Candidats et palettes
candidats <- c("Arthaud", "Poutou", "Roussel", "Mélenchon",
               "Jadot", "Hidalgo",
               "Macron", "Pécresse",
               "Lassalle", "Dupont-Aignan", "Le Pen", "Zemmour")
# dans certaines tables les `-` et ` ` sont remplacés par des `.` 
candidat2 <- c("Arthaud", "Poutou", "Roussel", "Mélenchon",
               "Jadot", "Hidalgo",
               "Macron", "Pécresse",
               "Lassalle", "Dupont.Aignan", "Le.Pen", "Zemmour")
# pour les résultats on ajoute abstention, blancs et nuls
liste_res <- function(liste_cand=candidats) return(c(liste_cand,
                      "abstention", "blancs", "nuls"))
# palettes
pal_pol <- c("#9d0d1699", "#ce292999", "#f1001c99", "#ff333399",
              "#23b73d99", "#ff668b99",
              "#ff9f0e99", "#0890c599",
              "#617b7599", "#02279c99", "#8d602699", "#65451999")
# pour les résultats on ajoute les trois couleurs abst / blcs / nls
pal_res <- function(pal_cand=pal_pol) return(c(pal_cand,
               "#bbbbbb99", "#eeeeee99", "#ffffff99"))